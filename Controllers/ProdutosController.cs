using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi00;

namespace WebApi00.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ProdutosController : ControllerBase
    {
        // GET: /Produtos
        [HttpGet]
        public IEnumerable<Produto> Get()
        {
            
            return new Produto[] { 
                new Produto { Nome="A", Preco=1}, 
                new Produto { Nome="B", Preco=2} 
                };
        }

       
    }
}