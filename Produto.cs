using System;

namespace WebApi00
{
    
    public class Produto
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();
        
        public string Codigo { get; set; }   

        public string Nome { get; set; } 

        public decimal Preco { get; set; } =0;
    }


}   