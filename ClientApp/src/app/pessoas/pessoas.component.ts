import { Component, OnInit, Inject } from '@angular/core';
import { IPessoa } from 'src/IPessoa';
import { HttpClient } from '@angular/common/http';
import { PessoaServiceService } from '../pessoa-service.service';

@Component({
  selector: 'app-pessoas',
  templateUrl: './pessoas.component.html',
  styleUrls: ['./pessoas.component.css']
})
export class PessoasComponent implements OnInit {
  pessoas : IPessoa [] = [
    {id:"1", nome:"A", apelido:"A", saldo:0},
    {id:"1", nome:"A", apelido:"A", saldo:0}
  ];
  
  _db:PessoaServiceService;
  
  constructor(db:PessoaServiceService) {
    this._db = db;
  }

  ngOnInit(): void {
    this._db.select().subscribe(
      result=>{
        this.pessoas = result;
      }
    );
  }

  addClicked() {
    var ii = {id:"1", nome:"A", apelido:"A", saldo:0};
    this._db.insert(ii);
  }

  delClicked() {
    var ii = {id:"1", nome:"A", apelido:"A", saldo:0};
    this._db.delete(ii.id);
  }
}
